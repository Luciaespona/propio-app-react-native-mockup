import React from 'react';
import { Text, View, Button } from 'react-native';
import { StackNavigator,} from 'react-navigation';
import { Asset, AppLoading } from 'expo';

// Redux
import { Provider } from 'react-redux'
import store from './reducers/index';

// Cuestom  import
import { styles } from './styles/AppStyles';

import AppNavigation from './navigation/AppNavigation'

// renders the splash screen while App is loading
// (currently a timer for test purposes), once
// finished, renders the initial screen

export default class App extends React.Component {

  state = {
    isReady: false,
  };

  // Display the splas screen untill everything is ready
  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          //startAsync={this._cacheResourcesAsync}
          startAsync={this._FakeAsync}
          //onFinish={() => this.setState({ isReady: true })}
          onFinish={() => console.log("Finished")}
          onError={console.warn}
        />
      );
    }
    // Start the screen navigator defined above
    return (
        <Provider store={store}>
           <AppNavigation/>
        </Provider>
    );
  }

 // Used to simulate loading time to see SplashScreen
  _FakeAsync = () => {
    console.log("Simulate loading time 1");
    setTimeout(function(){
      //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
      this.setState({ isReady: true });
      console.log("Simulate loading time 2");
    }.bind(this), 1000);
  }

  // Gets all the assets ready,
  // Server synchronization may go here too
  async _cacheResourcesAsync() {
    const images = [
      // test images
      require('./assets/test-img.png'),
      require('./assets/big-image.png'),
    ];

    const cacheImages = images.map((image) => {
      return Asset.fromModule(image).downloadAsync()
    });

    return await Promise.all(cacheImages)
  }
}
