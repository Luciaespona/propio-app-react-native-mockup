import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { styles } from '../styles/AppStyles';

export default class BlankScreen extends React.Component {
  static navigationOptions = {
    title: 'Blank Screen (TODO)',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Text> This is a 🛠 BLANK 🛠 screen </Text>
        <Text> 🌼 Replace me soon!! 🌼 </Text>
      </View>
    );
  }
}
