import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { StyleSheet } from 'react-native';

import { styles } from '../styles/AppStyles';

import LogoutContainer from  "../containers/LogoutContainer"

export default class LogoutScreen extends React.Component {

  static navigationOptions = {
    title: 'Log out',
  };

  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;

    return (
      <LogoutContainer navigate={navigate} />
    );
  }
}
