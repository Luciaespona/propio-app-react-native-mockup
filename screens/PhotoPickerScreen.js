import React, { Component } from 'react';
import { Text, Image, TouchableHighlight, ScrollView, View, Platform, Button, CameraRoll, Alert } from 'react-native';
import { StyleSheet } from 'react-native';
import {NavigationActions} from 'react-navigation';
import { styles } from '../styles/AppStyles';

import IconButton from '../components/IconButton';
import { COLOR_PRIMARY } from './../styles/common';

import testData from '../data/test-data-01.json';

export default class PhotoPickerScreen extends React.Component {

  static navigationOptions = {
    title: 'Pick Photo',
  };

  //Load test data
  componentWillMount(){
    this.setState({ photos: [], images:[] });
    this.getPhotos();
  }

  setIndex = (index) => {
    this.setState({ index: index });
    Alert.alert('Photo Selected', 'Select image ' + (index+1) + "?",
                 [ {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                   {text: 'OK', onPress: () => console.log('OK Pressed')},]);
    console.log("INDEX=" + this.state.index);
    console.log(this.state.photos[index].node.image.filename);
    console.log(this.state.photos[index].node.image.uri);
  }

  fetchPhotos = () => {
    testData.items.map(item => this.saveToCameraRoll(item.photo_url));
    return(testData.items.length)
  }

  saveToCameraRoll = (image_url) => {
    if (Platform.OS === 'android') {
      RNFetchBlob
        .config({
          fileCache : true,
          appendExt : 'jpg'
        })
        .fetch('GET', image_url)
        .then((res) => {
          CameraRoll.saveToCameraRoll(res.path())
            //.then(Alert.alert('Success', 'Photo added to camera roll!'))
            .catch(err => console.log('err:', err))
        })
    } else {
        CameraRoll.saveToCameraRoll(image_url)
          //.then(Alert.alert('Success', 'Photo added to camera roll!'))
    }
  }

  getPhotos = () => {

    CameraRoll.getPhotos({
           first:50,
           assetType: 'Photos',
         })
         .then(r => {
           this.setState({ photos: r.edges });
         })
         .catch((err) => {
            //Error Loading Images
         }).then(() =>  {console.log(this.state)});
  }

  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;

    return (
      <View style={[styles.container]}>
        <IconButton style={[styles.container]} imageRight title='Take Photo'
                    image={require('../assets/camera-white.png')}
                    />
        <ScrollView style={[{flex:0.5}]}>
             { this.state.photos.map((p, i) => {
                   return (
                     <TouchableHighlight
                        style={{opacity: i === this.state.index ? 0.5 : 1}}
                        key={i}
                        underlayColor='transparent'
                        onPress={() => this.setIndex(i)} >
                         <Image key={i} style={{ width: 200,height: 100}}
                           source={{ uri: p.node.image.uri }}/>
                    </TouchableHighlight>
                );
              })
            }
        </ScrollView>
        <View style={[styles.container, {flex:0.2, flexDirection:'row'}]}>
          <IconButton imageRight title='Fetch'
                     image={require('../assets/star-white.png')}
                     onPress={() => {this.fetchPhotos()}}/>
          <IconButton imageRight title='Cancel'
                     image={require('../assets/thumb-down-white.png')}
                     onPress={() => {this.props.navigation.dispatch(NavigationActions.back())}}/>
         </View>
      </View>
    );
  }
}
