import React, { Component } from 'react';
import { Text, View, Button, ActivityIndicator, TouchableHighlight, Image } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { styles } from '../styles/AppStyles';

import IconButton from '../components/IconButton'

export default class InitScreen extends React.Component {
  static navigationOptions = {
    title: 'Initial Screen',
  };

  // Simple scree that is shown first time one uses the app
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    //const name = params ? params.name : 'no name';

    //Prevent returning here
    const resetAction = NavigationActions.reset({
                          index: 0,
                          actions: [
                            NavigationActions.navigate({
                              routeName: "Register",
                              params: { }
                            })
                          ]
                        });
    return (
      <View style={styles.container}>
        <View style={styles.view_text}>
          <Text style={styles.text_big}> Keep track of your stuff </Text>
          <Text style={styles.text_big}> Find out what it worth$ </Text>
          <Text style={styles.text_big}> Secure it with one click</Text>
        </View>
        <View style={styles.flex_1}>
           <IconButton title='Got it!' image={require('../assets/thumb-up-white.png')}
                       onPress={() =>{this.props.navigation.dispatch(resetAction);}}/>
        </View>
      </View>
    );
  }
}
