import React, { Component } from 'react';
import { Text, View, Button, Image, Alert} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { styles } from '../styles/AppStyles';
import IconButton from '../components/IconButton'

export default class InitScreen extends React.Component {
  static navigationOptions = {
    title: 'Register Screen',
  };
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    //const name = params ? params.name : 'no name';

    return (
      <View style={styles.container}>
        <View style={[styles.view_text, {flex:0.2}]}>
          <Text style={[styles.text_big, {fontSize: 40}]}>  PROP.IO </Text>
        </View>
        <View style={styles.container}>
            <View style={styles.view_text}>
              <Text style={styles.text_big}> Please register first </Text>
              <Text style={styles.text_big}> It takes one minute!</Text>
            </View>
            <View style={styles.view_button_set}>
              <IconButton imageRight title='Register with Facebook' image={require('../assets/facebook-white.png')}
                        onPress={() =>{ Alert.alert("Facebook login under construction")}}/>
              <IconButton imageRight title='Log in with email' image={require('../assets/mail-white.png')}
                        onPress={() => navigate('Login', {})}/>
            </View>
          </View>
        </View>
    );
  }
}
