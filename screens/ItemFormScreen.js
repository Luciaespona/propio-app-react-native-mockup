import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { StyleSheet } from 'react-native';

import { styles } from '../styles/AppStyles';

import ItemFormContainer from '../containers/ItemFormContainer';

export default class ItemFormScreen extends React.Component {

  static navigationOptions = {
    title: 'Edit Item',
  };

  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    return (
      <View style={[styles.container]}>
        <View style={[styles.container, item_form_styles.itemForm]}>
          <ItemFormContainer navigate={navigate} item={params.item} />
        </View>
      </View>
    );
  }
}

const item_form_styles = StyleSheet.create({
  itemForm: {
    alignSelf:'stretch',
    alignItems:'stretch',
    justifyContent:'space-between',
    margin: 20,
    padding: 1,
  }
});
