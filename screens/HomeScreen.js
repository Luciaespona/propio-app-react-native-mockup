import React, { Component } from 'react';
import { Text, View, Image, TouchableHighlight,
         ActivityIndicator, FlatList } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { BackHandler } from 'react-native';
import { StyleSheet } from 'react-native';

import { styles } from '../styles/AppStyles';
import IconButton from '../components/IconButton';

import { connect } from 'react-redux';

import ItemListContainer from '../containers/ItemListContainer';

import { COLOR_PRIMARY } from './../styles/common';

class HomeScreen extends React.Component {
  constructor(props) {
		super(props);
	}

  handleBackPress = () => {
    return this.props.navigation.navigate('DrawerToggle');
  };
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;

    var userSurname = this.props.user.fullname.split(" ")[0];
    //const name = params ? params.name : 'no name';
    return (
      <View style={[styles.container]}>
        <View style={[styles.container, {flex:0.1}]}>
            <Text style={styles.text_big}> {userSurname}'s stuff </Text>
        </View>
        <View style={[styles.container, home_styles.swipeList]}>
          <ItemListContainer navigate={navigate}/>
        </View>
        <View style={[styles.container, {flex:0.2, flexDirection:'row'}]}>
          <IconButton imageRight title='Action'
                      image={require('../assets/star-white.png')}
                      onPress={() => navigate('Blank', {})}/>
          <IconButton imageRight title='Add'
                      image={require('../assets/plus-white.png')}
                      onPress={() => navigate('ItemForm', {})}/>
      </View>
    </View>
    );
  }
}

const home_styles = StyleSheet.create({
  swipeList: {
    alignSelf:'stretch',
    alignItems:'stretch',
    justifyContent:'space-between',
    margin: 10,
    padding: 1,
    backgroundColor: COLOR_PRIMARY,
  }
});

const mapStateToProps = (state) => ({
     user: state.loginReducer.user
});
export default connect(mapStateToProps)(HomeScreen);
