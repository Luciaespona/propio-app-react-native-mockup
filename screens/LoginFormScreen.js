import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { StyleSheet } from 'react-native';

import { styles } from '../styles/AppStyles';

import LoginFormContainer from '../containers/LoginFormContainer';

export default class LoginFormScreen extends React.Component {

  static navigationOptions = {
    title: 'Log in',
  };

  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    return (
      <View style={[styles.container]}>
        <View style={[styles.container, login_form_styles.itemForm]}>
          <LoginFormContainer navigate={navigate} dispatch={this.props.navigation.dispatch}/>
        </View>
      </View>
    );
  }
}

const login_form_styles = StyleSheet.create({
  itemForm: {
    alignSelf:'stretch',
    alignItems:'stretch',
    justifyContent:'space-between',
    margin: 20,
    padding: 1,
  }
});
