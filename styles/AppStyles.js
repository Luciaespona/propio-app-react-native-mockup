import { StyleSheet } from 'react-native';
import { COLOR_SECONDARY, COLOR_SECONDARY_TEXT } from './common';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_SECONDARY,
    alignItems: 'center',
    justifyContent: 'center',
  },

  flex_1:{
    flex:1,
  },

  view_text:{
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: 20,
  },

  text_big: {
      fontSize: 20,
      fontWeight: 'bold',
      color: COLOR_SECONDARY_TEXT,
    },
  text_small: {
    fontSize: 14,
    fontWeight: 'normal',
    color: COLOR_SECONDARY_TEXT,
  },

    view_button_set:{
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'flex-start',
      padding: 10,
    },
});
