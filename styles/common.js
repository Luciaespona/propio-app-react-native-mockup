export const COLOR_PRIMARY = 'teal';
export const COLOR_SECONDARY = '#d6f5f5';
export const COLOR_PRIMARY_TEXT = 'white';
export const COLOR_SECONDARY_TEXT = '#404040';
export const FONT_NORMAL = 'OpenSans-Regular';
export const FONT_BOLD = 'OpenSans-Bold';
export const BORDER_RADIUS = 5;
