import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { StyleSheet } from 'react-native';
import {Platform} from 'react-native';

import { Text, Image, TouchableHighlight, View, Button, TouchableOpacity } from 'react-native'
import React from 'react';

import BlankScreen from '../screens/BlankScreen';
import InitScreen from '../screens/InitScreen';
import HomeScreen from '../screens/HomeScreen';
import RegisterScreen from '../screens/RegisterScreen';
import LoginFormScreen from '../screens/LoginFormScreen';
import LogoutScreen from '../screens/LogoutScreen';
import ItemFormScreen from '../screens/ItemFormScreen';
import PhotoPickerScreen from '../screens/PhotoPickerScreen';

// login stack
const LoginStack = StackNavigator({
    Init: { screen: InitScreen },
    Register: { screen: RegisterScreen },
    Login: { screen: LoginFormScreen },
  },
  {
    navigationOptions: {
          gesturesEnabled: false,
      }
    }
);

const HomeStack = StackNavigator({
  Home: { screen: HomeScreen },
  ItemForm: { screen: ItemFormScreen },
  PhotoPicker: {screen: PhotoPickerScreen},
},
{
  // Default config for all screens
  headerMode: 'none',
});

const DrawerStack = DrawerNavigator({
  HomeStack: { screen: HomeStack },
  Blank: { screen: BlankScreen },
  Logout: { screen: LogoutScreen },
},
{
  // Default config for all screens
  headerMode: 'none',
  backBehavior: 'none',
});

const DrawerNavigation = StackNavigator({
  DrawerStack: { screen: DrawerStack }
}, {
  headerMode: 'float',
  navigationOptions: ({navigation}) => ({
    headerStyle: {paddingLeft: 10},
    title: 'Prop.io',
    headerTintColor: 'grey',
    headerLeft: Platform.OS === 'ios' ? <TouchableHighlight onPress={() => navigation.navigate('DrawerToggle')}>
                 <View>
                 <Image
                   source={require('../assets/drawer-black.png')}
                   style={{width: 20, height: 20,}}/> </View></TouchableHighlight>
                   : <View>
		                   <TouchableOpacity onPress={() =>  { navigation.navigate('DrawerToggle')} }>
                       <Image
                         source={require('../assets/drawer-black.png')}
                         style={{width: 20, height: 20,}}/>
                       </TouchableOpacity>
                     </View>
    })
});

// Manifest of possible screens
const AppNavigation = StackNavigator({
  loginStack: { screen: LoginStack },
  drawerStack: { screen: DrawerNavigation }
}, {
  // Default config for all screens
  headerMode: 'none',
  title: 'Main',
  initialRouteName: 'loginStack',
  navigationOptions: {
          gesturesEnabled: false,
      }
  });


export default AppNavigation
