import React, { Component } from 'react';
import { Text, ScrollView, View, Platform, Button } from 'react-native';
import { StyleSheet } from 'react-native';
import { styles } from '../styles/AppStyles';

import IconButton from '../components/IconButton';
import { COLOR_PRIMARY } from './../styles/common';

import testData from '../data/test-data-01.json';

import t from 'tcomb-form-native'; // 0.6.9

var Form = t.form.Form;

var Item = t.struct({
  name: t.String,
  category: t.maybe(t.String),
  brand: t.maybe(t.String),
  value: t.maybe(t.Number),
  insured: t.Boolean,
  date: t.maybe(t.String),
  encoded: t.maybe(t.String),
});

const formStyles = {
  ...Form.stylesheet,
  textbox: {
    ...Form.stylesheet.textbox,
    normal: {
      ...Form.stylesheet.textbox.normal,
      color: 'blue',
      borderColor: COLOR_PRIMARY,
      borderWidth: 2,
      backgroundColor: 'white',
    },
    error: {
      ...Form.stylesheet.textbox.error,
      borderWidth: 2,
      backgroundColor: '#ffe6e6',
    }
  },
};

const options = {
  stylesheet: formStyles,
  fields: {
    insured: {
      label: 'Protected?',
    },
    value: {
      error: 'An estimated numeric value of what it worths is important'
    },
    brand: {
      error: 'Introduce the brand and model if applicable'
    },
  },
};

export default class ItemFormComponent extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount(){
    var RandomNumber = Math.floor(Math.random() * testData.items.length) ;
    this.props.add({ ...testData.items[RandomNumber], insured:false });
  }

  handleSubmit = () => {
    const item = this._form.getValue(); // use that ref to get the form value
    if (item) {
      if (this.props.item != undefined) {
        this.props.edit({ ...item, id:this.props.item.id });
      }
      else {
          this.props.add(item);
      }
      this.props.navigate('Home', {});
    }
  }

  render() {
    return (
      <ScrollView>
        <IconButton imageRight title='Get Photo'
                    image={require('../assets/camera-white.png')}
                    onPress={() => {this.props.navigate('PhotoPicker', {})}}/>
        <Form
          ref={c => this._form = c} // assign a ref
          type={Item}
          options={options} // pass the options via props
          value={this.props.item}
        />
        <View style={[styles.container, {flex:0.2, flexDirection:'row'}]}>
          <IconButton imageRight title='Sumbit'
                      image={require('../assets/thumb-up-white.png')}
                      onPress={this.handleSubmit}/>
          <IconButton imageRight title='Cancel'
                      image={require('../assets/thumb-down-white.png')}
                      onPress={() => {this.props.navigate('Home', {})}}/>
        </View>
      </ScrollView>
    );
  }
}
