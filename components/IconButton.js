
import React, { Component } from 'react';
import { Text, View, TouchableHighlight, Image } from 'react-native';

import { StyleSheet } from 'react-native';
import { COLOR_PRIMARY, COLOR_PRIMARY_TEXT } from './../styles/common';

export default class IconButton extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    if(this.props.imageRight) {
      return (
        <TouchableHighlight
           style={styles.button_touch}
           onPress={() => {this.props.onPress()}}>
          <View style={styles.view_button}>
            <Text adjustsFontSizeToFit={true} style={styles.text_button}> {this.props.title} </Text>
            <Image style={styles.image_button_right} source={this.props.image} />
          </View>
        </TouchableHighlight>
      )
    }
    return (
      <TouchableHighlight
         style={styles.button_touch}
         onPress={() => {this.props.onPress()}}>
        <View style={styles.view_button}>
          <Image style={styles.image_button} source={this.props.image}/>
          <Text adjustsFontSizeToFit={true} style={styles.text_button}> {this.props.title} </Text>
        </View>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({

  button_touch: {
    flex: 0,
    height: 50,
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 15,
    borderRadius: 25,
    backgroundColor: COLOR_PRIMARY,
    margin: 10,
  },

  view_button:{
      flex: 1,
      flexDirection: 'row',
    },
  image_button: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    marginLeft: 2,
    marginBottom: 2,
  },
  image_button_right: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    marginLeft: 7,
    marginBottom: 2,
  },
  text_button: {
    height: 25,
    fontSize: 20,
    fontWeight: 'bold',
    color: COLOR_PRIMARY_TEXT,
    alignSelf: 'center',
    textAlign:'center',
    justifyContent: 'center',
    textAlignVertical: "center",
  },
})
