import React, { Component } from 'react';
import { Text, View, Platform, Alert } from 'react-native';
import { StyleSheet } from 'react-native';
import { styles } from '../styles/AppStyles';

import userdata from "../data/test-user-data-01.json"

import IconButton from '../components/IconButton';
import { COLOR_PRIMARY, COLOR_SECONDARY_TEXT } from './../styles/common';

import { NavigationActions } from 'react-navigation';

import t from 'tcomb-form-native'; // 0.6.9

var Form = t.form.Form;

var LoginData = t.struct({
  email: t.String,
  password: t.String,
  remember: t.Boolean,
});

const formStyles = {
  ...Form.stylesheet,
  controlLabel:{
    ...Form.stylesheet.controlLabel,
    normal: {
      ...Form.stylesheet.controlLabel.normal,
      fontWeight:'bold',
      color: COLOR_SECONDARY_TEXT,
    },
  },
  textbox: {
    ...Form.stylesheet.textbox,
    normal: {
      ...Form.stylesheet.textbox.normal,
      color: 'black',
      borderColor: COLOR_PRIMARY,
      borderWidth: 2,
      backgroundColor: 'white',
    },
    error: {
      ...Form.stylesheet.textbox.error,
      borderWidth: 2,
      backgroundColor: '#ffe6e6',
    }
  },
};

const options = {
  stylesheet: formStyles,
  fields: {
    email: {
      placeholder: 'mail@domain.com',
      error: 'Please introduce a valid email',
    },
    password: {
      secureTextEntry: true,
      error: 'Introduce you password (case sensitive)'
    },
    remember: {
      label: 'Stay logged in?',
    },
  },
};

export default class LoginFormComponent extends React.Component {

  constructor(props) {
    super(props);
  }

  simulateLogin = (data) => {
    var login_user = false;
    userdata.users.map(user =>
      (user.email.toLowerCase() === data.email.toLowerCase())
     //&& (user.password === action.data.password)
      ? login_user ={ ...user } : null );
      return login_user;
  }

  handleSubmit = () => {
    const data = this._form.getValue(); // use that ref to get the form value
    console.log('login data: ', data);
    if (data) {
        var user = this.simulateLogin(data);
        if (user) {
          this.props.login(user);
          this.props.navigate('drawerStack', {});
        }
        else{
          Alert.alert("Bad Login Data, please try again");
        }
      }
 }

  render() {
      var initial_user = (this.props.user ? this.props.user :{email:"lucia.espona@rodamen.com", password:"x"})
      return (
        <View>
          <Form
            ref={c => this._form = c} // assign a ref
            type={LoginData}
            options={options} // pass the options via props
            value={initial_user}
          />
          <IconButton imageRight title='Sumbit'
                      image={require('../assets/thumb-up-white.png')}
                      onPress={ this.handleSubmit }/>
        </View>
      );
  }
}
