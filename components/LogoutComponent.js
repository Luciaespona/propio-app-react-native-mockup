import React, { Component } from 'react';
import { Text, View } from 'react-native';

import { styles } from '../styles/AppStyles';
import IconButton from '../components/IconButton';

import { NavigationActions } from 'react-navigation';

export default class LogoutComponent extends React.Component {

  constructor(props) {
    super(props);
  }

  handleLogout = () => {
    this.props.logout();
    this.props.navigate('loginStack', {});
 }

  render() {
      if (this.props.user) {
        return (
          <View style={[styles.container]}>
            <Text style={styles.text_big}> Currently logged in as {this.props.user.fullname}</Text>
            <IconButton imageRight title='Logout'
                        image={require('../assets/logout-white.png')}
                        onPress={ this.handleLogout }/>
          </View>
        );
      }
      else {
        return (<Text> Logging out... </Text>);
      }
  }
}
