
import React, { Component } from 'react';
import { Text, View, Image, TouchableHighlight } from 'react-native';
import { SwipeListView } from 'react-native-swipe-list-view';

import { StyleSheet } from 'react-native';
import { COLOR_PRIMARY, COLOR_PRIMARY_TEXT, BORDER_RADIUS,
         COLOR_SECONDARY, COLOR_SECONDARY_TEXT } from './../styles/common';


export default class SwipeItemList extends React.Component {
  constructor(props) {
    super(props)
  }

  renderListItem(item){
    let item_photo = require('../assets/no-image-grey.png');

    if (item.encoded) {
      item_photo = {uri: 'data:image/png;base64,' + item.encoded };
    }

    let insured = 'false';
    if (item.insured != undefined && item.insured) {insured = 'true'}

    return(
      <View style={[styles.rowFront,]}>
        <View>
          <Text style={[styles.rowFront_text, styles.text_big]}>{item.name} ({item.category})</Text>
          <Text style={[styles.rowFront_text, styles.text_small]}>{item.date}, {item.brand}</Text>
          <Text style={[styles.rowFront_text, styles.text_small]}>{insured}, {item.value}</Text>
        </View>
        <Image
             resizeMode='cover'
             style={styles.rowImage}
             source={item_photo}
         />
      </View>
    )
  }

  render() {
    return (
      <SwipeListView
           useFlatList
           keyExtractor={(item, index) => 'index_' + item.id}
           data={this.props.items}
           renderItem={ (data, rowMap) =>
             this.renderListItem(data.item) }
           renderHiddenItem={ (data, rowMap) => (
              <View style={styles.rowBack}>
                <TouchableHighlight style={{ padding:20, paddingLeft:5}}
                  onPress={() => this.props.navigate('ItemForm', {item:data.item})}>
                  <Text style={styles.rowBack_text} > EDIT </Text>
                </TouchableHighlight>
                <TouchableHighlight style={{ padding:20, paddingRight:5}}
                  onPress={() => this.props.delete(data.item.id)}>
                  <Text style={styles.rowBack_text} > DELETE </Text>
                </TouchableHighlight>
               </View>
           )}
           leftOpenValue={75}
           rightOpenValue={-75}
       />
    )
  }
}

const styles = StyleSheet.create({
  rowImage: {
    borderColor:'#e6e6e6',
    borderWidth:2,
    borderRadius: BORDER_RADIUS,
    padding:5,
    width: 90,
    height: 90,
  },
  rowFront: {
    borderColor: COLOR_PRIMARY,
    borderWidth: 2,
    borderRadius: BORDER_RADIUS,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
    borderBottomColor: COLOR_PRIMARY,
    borderBottomWidth: 3,
    //justifyContent: 'center',
    justifyContent: 'space-between',
    height: 100,
    padding:5,
  },

  rowFront_text: {
    textAlign:'left',
    //borderColor: 'red',
    //borderWidth: 1,
  },

  rowBack: {
    alignItems: 'center',
    backgroundColor: COLOR_PRIMARY,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 0,
    paddingRight: 0,
  },

  rowBack_text: {
    color: COLOR_PRIMARY_TEXT,
    fontWeight: 'bold',
  },
  text_big: {
      fontSize: 20,
      fontWeight: 'bold',
      color: COLOR_SECONDARY_TEXT,
    },
  text_small: {
    fontSize: 14,
    fontWeight: 'normal',
    color: COLOR_SECONDARY_TEXT,
  },

})
