export const LIST_CLEAR = 'list_clear'
export const ITEM_ADD = 'item_add'
export const ITEM_DELETE = 'item_delete'
export const ITEM_EDIT = 'item_edit'

export const LOGIN = 'login'
export const LOGOUT = 'logout'
