import * as Actions from './ActionTypes';

let nextItemId = 0

export const clearList = () => ({
    type: Actions.LIST_CLEAR,
});

export const addItem = item => ({
    type: Actions.ITEM_ADD,
    id: nextItemId++,
    item
});

export const editItem = item => ({
    type: Actions.ITEM_EDIT,
    item
});

export const deleteItem = id => ({
    type: Actions.ITEM_DELETE,
    id
});

export const login = user => ({
    type: Actions.LOGIN,
    user
});

export const logout = () => ({
    type: Actions.LOGOUT
});
