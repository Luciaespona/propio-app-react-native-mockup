import * as Actions from '../actions/ActionTypes'

const LoginReducer = (state = {}, action) => {
  switch (action.type) {
    case Actions.LOGOUT:
      return ({})
    case Actions.LOGIN:
      return ({user:{ ...action.user }})
    default:
      return (state)
  }
}

export default LoginReducer;
