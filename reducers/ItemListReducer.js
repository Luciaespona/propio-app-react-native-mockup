import * as Actions from '../actions/ActionTypes'

const ItemListReducer = (state = [], action) => {
  switch (action.type) {
    case Actions.LIST_CLEAR:
      return []
    case Actions.ITEM_ADD:
      return [
        ...state, { ...action.item,
          id: action.id
        }
      ]
    case Actions.ITEM_EDIT:
      return (
        state.map(old_item => old_item.id === action.item.id ?
                            { ...action.item } : old_item))
    case Actions.ITEM_DELETE:
      return (state.filter(item => item.id !== action.id))

    default:
      return state;
  }
}

export default ItemListReducer;
