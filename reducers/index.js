import {
  combineReducers, applyMiddleware, createStore
}
from 'redux';
import logger from 'redux-logger';

import itemListReducer from './ItemListReducer';
import loginReducer from './LoginReducer';

const AppReducers = combineReducers({
  itemListReducer,
  loginReducer,
});

const rootReducer = (state, action) => {
  return AppReducers(state, action);
}

let store = createStore(rootReducer, applyMiddleware(logger));

export default store;
