import { connect } from 'react-redux';

import { editItem, deleteItem} from '../actions'
import SwipeItemList from '../components/SwipeItemList';

const mapStateToProps = (state) => ({
     items: state.itemListReducer
});

const mapDispatchToProps = (dispatch) => ({
  delete: (id) => dispatch(deleteItem(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SwipeItemList);
