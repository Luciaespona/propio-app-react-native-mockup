import { connect } from 'react-redux';

import { logout, clearList } from '../actions'
import LogoutComponent from '../components/LogoutComponent';

const mapStateToProps = (state) => ({
     user: state.loginReducer.user
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => Promise.resolve([ dispatch(logout()), dispatch(clearList())]),
});

export default connect(mapStateToProps, mapDispatchToProps)(LogoutComponent);
