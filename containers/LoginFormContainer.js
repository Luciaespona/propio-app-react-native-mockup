import { connect } from 'react-redux';

import { login } from '../actions'
import LoginFormComponent from '../components/LoginFormComponent';

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
  login: (data) => dispatch(login(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginFormComponent);
