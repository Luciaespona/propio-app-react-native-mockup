import { connect } from 'react-redux';

import { addItem, editItem } from '../actions'
import ItemFormComponent from '../components/ItemFormComponent';

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
  edit: (item) => dispatch(editItem(item)),
  add: (item) => dispatch(addItem(item)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemFormComponent);
